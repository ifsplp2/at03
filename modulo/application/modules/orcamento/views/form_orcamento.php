

<div class="text-center border border-light p-5" style="background-color: #F8F8FF;">

    
        <p class="h4 mb-4">Gerador de PDF</p>

        <!-- Nome -->
        <input type="text" class="form-control mb-4" placeholder="Nome do Cliente" v-model="nome">

        <!-- Email -->
        <input type="email" class="form-control mb-4" placeholder="E-mail" v-model="email">

        <?= $produto ?>

        <div>
            <?= $lista_produtos ?> 
        </div>    

        <!-- Total -->
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Total</span>
            </div>
                <input type="number" id="total" class="form-control text-center" placeholder="Total" aria-label="Username" aria-describedby="basic-addon1" v-model="total" readonly="true">
            <div class="input-group-append">
                <span class="input-group-text">R$</span>
            </div>
        </div>   

        <!-- Button -->
        <button  class="btn btn-info btn-block mt-5" @click="checkForm()">Gerar</button>
    
</div>

