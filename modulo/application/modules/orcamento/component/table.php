<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class table
{
    private $produtos;

    function __construct($produtos)
    {
        $this->produtos = $produtos;
    }

    public function getHTML()
    {
        $html = '
        <table id="dtBasicExample" class="table table-striped display responsive nowrap" style="width:100%">
        <thead>
          <tr>
              <th class="th-sm text-center" data-priority="1">Nome
              </th>  
              <th class="th-sm text-center">Valor
              </th>                   
          </tr>
        </thead>
        <tbody>     
          
        ';
        foreach($this->produtos as $produto)
        {
            $html .= '      
            <tr>         
                <td class="th-sm text-center">
                    '.$produto->nome.'
                </td>
                <td class="text-center">
                    '.$produto->preco.' 
                </td>                                         
            </tr>';        
        }

        $html .= '
        </tbody> 
        </table>';
           
        return $html;
    }
}