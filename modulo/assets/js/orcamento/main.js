new Vue({
    el:'#app',

    data: {
        tituloPagina: "Orçamentos",
        nome:"",
        email:"",        
        produtoNome: "",
        produtoPreco: 0,
        produtos: [],
        errors: [],
    },

    methods: {
        adcionarProduto(){
            this.produtos.push({
                nome: this.produtoNome,
                preco: this.produtoPreco,
            })
        },

        removerProduto(index){
            this.produtos.splice(index,1)
        },

        salvar()
        {            
            var formdata=new FormData();
            formdata.append("dados", JSON.stringify({nome: this.nome, email: this.email, 
                total : this.total, produtos : this.produtos}));

            return axios.post('/modulo/orcamento/guardarSession/', formdata)
            .then(response => {
                if(response){
                    window.location.href = 'Orcamento/gerarPdf'      
                } 
            })
            .catch(error => {
                console.log(error.response)
            })
        },

        checkForm: function (e) {
            this.errors = [];
            
            if (!this.nome) {
              this.errors.push('O nome é obrigatório.');
            }
            if (!this.email) {
              this.errors.push('O e-mail é obrigatório.');
            } else if (!this.validEmail(this.email)) {
              this.errors.push('Utilize um e-mail válido.');
            }
            if (this.produtos.length < 1) {
                this.errors.push('É necessario pelo menos um produto');
              }
      
            if (!this.errors.length) {
                this.salvar()
            }              
          },

          validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          }        
    },

    computed: {
        total(){
            t = 0

            this.produtos.forEach(produto => {
                t += parseFloat(produto.preco, 10)
            });
            
            return t;
        }
    }
})