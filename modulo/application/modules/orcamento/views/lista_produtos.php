<div class="m-5">
    <h4 class="text-center mt-5">Produtos Inseridos</h4>
    <hr/>   
        <div class="row"> 
        <div class="col-8">           
            <p class="text-left" v-for="produto in produtos">{{produto.nome}}</p>
        </div>
        <div class="col-2">            
            <p class="text-left" v-for="produto in produtos">{{produto.preco}} R$</p>            
        </div>
        <div class="col-2">            
            <p class="text-left" v-for="(produto,i) in produtos"><i class="fas fa-times" @click="removerProduto(i)"></i></p>            
        </div>
</div>
