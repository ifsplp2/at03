<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pdf
{
    private $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function getHTML()
    {
        return $html = '<div class="container">
            <h1 class="text-center mt-2">Orçamento</h1>
            <hr/>
        
            <div class="row m-4">
                <div class="col-6">
                    <p>Orçamento n° 1010</p>
                </div>
                <div class="col-6">
                    <p class="text-right">contato: (11)6666-6666</p>
                </div>
            </div>
            <hr>
        
            <div>
                <p><span class="font-weight-bold">Cliente:</span> '.$data["nome"].'</p>
                <p><span class="font-weight-bold">E-mail:</span> '.$data["email"].'</p>
            </div>
            <hr/>
        
            <div class="row text-center text-info">
                '.$data["tabela"].'
            </div>
        
            <div class="text-info">
                <hr/>
                <span class="font-weight-bold"><p class="text-right">Total: '.$data["total"].' R$ </span> </p>
            </div>
        </div>';
    }
}