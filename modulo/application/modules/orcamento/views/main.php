<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<div id="app"class="container d-flex justify-content-md-center m-5">        
    <div class="row-12">
    <h1 class="text-center">{{tituloPagina}}</h1>
    <div v-if="errors.length" class="red lighten-4 p-4">
        <b>Por favor, corrija o(s) seguinte(s) erro(s):</b>
        <ul>
            <li v-for="error in errors">{{ error }}</li>
        </ul>
    </div>
        <?= $form_orcamento ?>  
    </div>      
</div>

<script type="text/javascript" src="<?= base_url() ?>assets/js/orcamento/main.js"></script>


