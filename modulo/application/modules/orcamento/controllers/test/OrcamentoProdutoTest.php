<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/orcamento/libraries/OrcamentoProduto.php';

/**
 * Essa classe de teste é apenas ilustrativa e foi criada somente 
 * para que você perceba a utilidade da execução de uma suite de testes.
 */

class OrcamentoProdutoTest extends Toast{
    
    function __construct(){
        parent::__construct('OrcamentoProdutoTest');
    } 
    
    function test_nome_nao_eh_vazio()
    {
        $orcamento = new OrcamentoProduto("Geraldo","gerald@bruxo.com", array(
            array('nome' => 'pera', 'preco' => 5),
            array('nome' => 'uva', 'preco' => 3)), 
        );

        $this->_assert_equals("Geraldo", $orcamento->getNome(), "Valor Calculado Incorreto");
    }

    function test_e_email_nao_eh_vazio()
    {
        $orcamento = new OrcamentoProduto("Geraldo","gerald@bruxo.com", array(
            array('nome' => 'pera', 'preco' => 5),
            array('nome' => 'uva', 'preco' => 3)), 
        );

        $this->_assert_equals("gerald@bruxo.com", $orcamento->getEmail(), "Valor Calculado Incorreto");
    }

    function test_resultado_valor_total(){

        $orcamento = new OrcamentoProduto("Geraldo","gerald@bruxo.com", array(
            array('nome' => 'pera', 'preco' => 5),
            array('nome' => 'uva', 'preco' => 3)), 
        );
        
        $this->_assert_equals(8, $orcamento->calcularTotal(), "Valor Calculado Incorreto");
    }

    function test_tem_pelo_menos_um_produto(){
        $orcamento = new OrcamentoProduto("Geraldo","gerald@bruxo.com", array(
            array('nome' => 'pera', 'preco' => 5),
            array('nome' => 'uva', 'preco' => 3)), 
        );

        $this->_assert_equals(2, $orcamento->countProdutos(), "Valor Calculado Incorreto");
    }
}