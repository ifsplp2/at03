<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class OrcamentoProduto{

    private $nome;
    private $email;
    private $total;
    private $produtos;

    function __construct($nome, $email, $produtos){
        $this->nome = $nome;
        $this->email = $email;
        $this->produtos = $produtos;
        $this->total = $this->calcularTotal();
    }

    public function calcularTotal(){

        $total = 0;
        
        foreach($this->produtos as $produto)
        {
            $total += $produto['preco'];
        }

        return $total;
    }

    public function countProdutos()
    {
        return count($this->produtos);
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getEmail()
    {
        return $this->email;
    }
}