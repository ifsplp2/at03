Felipe P. Claro - gu3002284

### Orçamento

- Gerador de PDF's de orçamentos.
- Validações dos campos de Nome, Email e Produtos.
- Ao adicionar produtos eles são redenrizados na lista, e seu valor somado ao valor total.
- É possível remover produtos da lista.
- Testes unitarios: "~/modulo/orcamento/test/OrcamentoProdutoTest"

### Bug's

- A biblioteca utlizada não suporta a formatação do mdbootstrap, gerando o pdf sem todas configurações predefinidas.

![](http://portal.ifspguarulhos.edu.br/images/logos/Guarulhos-02.jpg)
