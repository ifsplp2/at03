<?php
defined('BASEPATH') OR exit('No direct script acess allowed');
include APPPATH.'modules/orcamento/component/table.php';
include APPPATH.'modules/orcamento/component/pdf.php';

class OrcamentoModel extends CI_Model
{   
    public function setPdfData($dados)
    {            
        $dados = json_decode($dados);

        $data['session_id']  = 666;
        $data['nome'] = $dados->nome;
        $data['email'] = $dados->email;
        $data['total'] = $dados->total;
        $data['tabela'] = $this->table($dados->produtos);

        return $data;
    }

    public function table($produtos)
    {       
        $table = new table($produtos);

        return $table->getHTML();
    }

    public function pdf($data)
    {
        // $pdf = new pdf($data);
        //return $pdf->getHTML();

        return $html = ' 
        <!DOCTYPE html>
        <html lang="en">

        <head>

            <meta charset="utf-8">    
            <title>AT03 - HMVC</title>    
            <link href="'.base_url().'assets/mdb/css/mdb.min.css" rel="stylesheet">
            <link href="'.base_url().'assets/mdb/css/style.css" rel="stylesheet">
        </head>

        <body class="full-height">     
                <div class="container">
                    <h1 class="text-center mt-2">Orçamento</h1>
                    <hr/>
                
                    <div class="row m-4">
                        <div class="col-6">
                            <p>Orçamento n° 1010</p>
                        </div>
                        <div class="col-6">
                            <p class="text-right">contato: (11)6666-6666</p>
                        </div>
                    </div>
                    <hr>
                
                    <div>
                        <p><span class="font-weight-bold">Cliente:</span> '.$data["nome"].'</p>
                        <p><span class="font-weight-bold">E-mail:</span> '.$data["email"].'</p>
                    </div>
                    <hr/>
                
                    <div class="row text-center text-info">
                        '.$data["tabela"].'
                    </div>
                
                    <div class="text-info">
                        <hr/>
                        <span class="font-weight-bold"><p class="text-right">Total: '.$data["total"].' R$ </span> </p>
                    </div>
                </div>
                </body>

        </html>';
    }   
}