<?php 
require_once 'vendor\autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Orcamento extends MY_Controller{ 

    public function __construct(){
        $this->load->model('OrcamentoModel', 'model');
        $this->load->library('session');
    }
     
    public function index(){

        $data['lista_produtos'] = $this->load->view('lista_produtos', '',true);
        $data['produto'] = $this->load->view('produto','',true);
        $data['form_orcamento'] = $this->load->view('form_orcamento',$data, true);       

        $html = $this->load->view('main', $data, true);
        $this->show($html);        
    }

    public function guardarSession()
    {        
        $dados = $this->input->post('dados', true);   
                
        $data = $this->model->setPdfData($dados);  
            
        $_SESSION['data'] = $data;
        
        return true;
        // redirect('/Orcamento/teste/');        
    }

    public function gerarPdf()
    {       
        $html = $this->model->pdf($_SESSION['data']);
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);      
        $mpdf->Output();
        //$this->show($html); 
        unset($_SESSION['data']);
        session_destroy();
    }
}