<div class="input-group mb-3">    
    <div class="row">
        <div class="col-7">
            <input type="text" aria-label="Produto" class="form-control" placeholder="Produto" v-model="produtoNome">
        </div>
        <div class="col-2">
            <input type="number" aria-label="Preço" class="form-control ml-2 mr-2" placeholder="Preço" min="0" v-model="produtoPreco">
        </div>
        <div class="col-3">
            <div class="input-group-apppend">
                <button class="btn btn-info btn-outline-primary m-0 px-3 py-2 z-depth-0 waves-effect" type="button" id="button-addon1" @click="adcionarProduto()">Adicionar</button>
            </div>
        </div>
    </div>            
</div>